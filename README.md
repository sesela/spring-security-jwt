# spring-security-jwt

## Gradleでの取り込み方
buid.gradleファイルを下記のとおり修正。Mavenの場合も読み替えて設定してください。

```buid.gradle
repositories {
    mavenCentral()
    maven {url 'https://gitlab.com/sesela/spring-security-jwt/raw/master/repos'}  // ←追加
}

dependencies {
    compile group: 'com.keyportsolutions', name: 'spring-security-jwt', version: '1.0.0'  // ←追加
}
```
