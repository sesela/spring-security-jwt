package com.keyportsolutions.security.auth.jwt.dataholder;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * ユーザパスワード認証リクエストクラス
 */
public class UserPasswordAuthRequest {

	/** ユーザ名 */
	private String username;
	/** パスワード */
	private String password;

	/**
	 * コンストラクタ
	 * @param username ユーザ名
	 * @param password パスワード
	 */
	@JsonCreator
	public UserPasswordAuthRequest(@JsonProperty("username") String username, @JsonProperty("password") String password) {
		this.username = username;
		this.password = password;
	}

	/**
	 * ユーザ名を取得します。
	 * @return ユーザ名
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * パスワードを取得します。
	 * @return パスワード
	 */
	public String getPassword() {
		return password;
	}
}
