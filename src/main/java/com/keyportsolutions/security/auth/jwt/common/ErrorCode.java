package com.keyportsolutions.security.auth.jwt.common;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * エラーコードEnum
 */
public enum ErrorCode {

	/** 認証エラー */
	AUTHENTICATION(10),

	/** 失効エラー */
	JWT_TOKEN_EXPIRED(11);

	/** エラーコード **/
	private int errorCode;

	/**
	 * コンストラクタ
	 * @param errorCode エラーコード
	 */
	ErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * エラーコードを取得します。
	 * @return エラーコード
	 */
	@JsonValue
	public int getErrorCode() {
		return errorCode;
	}
}
