package com.keyportsolutions.security.auth.jwt.common;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.savedrequest.SavedRequest;

/**
 * ユーティリティクラス
 */
public final class WebUtil {

	/** XML_HTTP_REQUEST */
	private static final String XML_HTTP_REQUEST = "XMLHttpRequest";

	/** X_REQUESTED_WITH */
	private static final String X_REQUESTED_WITH = "X-Requested-With";

	/** CONTENT_TYPE */
	private static final String CONTENT_TYPE = "Content-type";

	/** CONTENT_TYPE_JSON */
	private static final String CONTENT_TYPE_JSON = "application/json";

	/**
	 * コンストラクタ
	 */
	private WebUtil() {
		super();
	}

	/**
	 * Ajaxからのリクエストかチェックします。
	 * @param request リクエスト
	 * @return チェック結果
	 */
	public static boolean isAjax(HttpServletRequest request) {
		return XML_HTTP_REQUEST.equals(request.getHeader(X_REQUESTED_WITH));
	}

	/**
	 * Ajaxからのリクエストかチェックします。
	 * @param request リクエスト
	 * @return チェック結果
	 */
	public static boolean isAjax(SavedRequest request) {
		return request.getHeaderValues(X_REQUESTED_WITH).contains(XML_HTTP_REQUEST);
	}

	/**
	 * Jsonのリクエストかチェックします。
	 * @param request リクエスト
	 * @return チェック結果
	 */
	public static boolean isContentTypeJson(SavedRequest request) {
		return request.getHeaderValues(CONTENT_TYPE).contains(CONTENT_TYPE_JSON);
	}
}
