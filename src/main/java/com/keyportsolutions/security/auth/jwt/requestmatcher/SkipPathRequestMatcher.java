package com.keyportsolutions.security.auth.jwt.requestmatcher;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.Assert;

/**
 * 対象外パスを考慮したRequestMatcherクラス
 */
public class SkipPathRequestMatcher implements RequestMatcher {

	/** 処理対象外パスのMatcher */
	private OrRequestMatcher skipMatcher;

	/** 処理対象パスのMatcher */
	private RequestMatcher processingMatcher;

	/**
	 * コンストラクタ
	 * @param pathsToSkip 処理対象外パスリスト
	 * @param processingPath 処理対象パス
	 */
	public SkipPathRequestMatcher(List<String> pathsToSkip, String processingPath) {
		Assert.notNull(pathsToSkip);
		List<RequestMatcher> m = pathsToSkip.stream().map(path -> new AntPathRequestMatcher(path))
				.collect(Collectors.toList());
		skipMatcher = new OrRequestMatcher(m);
		processingMatcher = new AntPathRequestMatcher(processingPath);
	}

	/**
	 * 下記順でチェックします。
	 * <ol>
	 *   <li>リクエストが処理対象外パスリストに含まれていればfalseを返します。
	 *   <li>リクエストが処理対象パスに含まれているか チェックして結果を返します。
	 * </ol>
	 */
	@Override
	public boolean matches(HttpServletRequest request) {
		if (skipMatcher.matches(request)) {
			return false;
		}
		return processingMatcher.matches(request) ? true : false;
	}
}
