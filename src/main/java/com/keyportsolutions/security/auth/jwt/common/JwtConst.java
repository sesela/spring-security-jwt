package com.keyportsolutions.security.auth.jwt.common;

/**
 * JWT定数クラス
 */
public final class JwtConst {

	/**
	 * コンストラクタ
	 */
	private JwtConst() {
		super();
	}

	/** JWTトークンヘッダ */
	public static final String JWT_TOKEN_HEADER_PARAM = "X-Authorization";
}
