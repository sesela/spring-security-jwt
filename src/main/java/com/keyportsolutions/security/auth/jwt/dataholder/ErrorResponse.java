package com.keyportsolutions.security.auth.jwt.dataholder;

import java.util.Date;

import org.springframework.http.HttpStatus;

import com.keyportsolutions.security.auth.jwt.common.ErrorCode;

/**
 * エラーレスポンスクラス
 */
public class ErrorResponse {

	/** HTTPステータス */
	private final HttpStatus httpStatus;
	/** メッセージ */
	private final String message;
	/** エラーコード */
	private final ErrorCode errorCode;
	/** タイムスタンプ */
	private final Date timestamp;

	/**
	 * コンストラクタ
	 * @param message メッセージ
	 * @param errorCode エラーコード
	 * @param httpStatus HTTPステータス
	 */
	protected ErrorResponse(final String message, final ErrorCode errorCode, HttpStatus httpStatus) {
		this.message = message;
		this.errorCode = errorCode;
		this.httpStatus = httpStatus;
		this.timestamp = new java.util.Date();
	}

	/**
	 * エラーレスポンスを生成します。
	 * @param message メッセージ
	 * @param errorCode エラーコード
	 * @param httpStatus HTTPステータス
	 * @return エラーレスポンス
	 */
	public static ErrorResponse createErrorResponse(final String message, final ErrorCode errorCode, HttpStatus httpStatus) {
		return new ErrorResponse(message, errorCode, httpStatus);
	}

	/**
	 * HTTPステータスを取得します。
	 * @return HTTPステータス
	 */
	public Integer getHttpStatus() {
		return httpStatus.value();
	}

	/**
	 * メッセージを取得します。
	 * @return メッセージ
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * エラーコードを取得します。
	 * @return エラーコード
	 */
	public ErrorCode getErrorCode() {
		return errorCode;
	}

	/**
	 * タイムスタンプを取得します。
	 * @return タイムスタンプ
	 */
	public Date getTimestamp() {
		return timestamp;
	}
}
