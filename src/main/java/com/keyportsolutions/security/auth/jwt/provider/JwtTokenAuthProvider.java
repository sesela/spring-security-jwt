package com.keyportsolutions.security.auth.jwt.provider;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.keyportsolutions.security.auth.jwt.authtoken.JwtAuthToken;
import com.keyportsolutions.security.auth.jwt.dataholder.JwtTokenFactory;
import com.keyportsolutions.security.auth.jwt.dataholder.RequestJwtAccessToken;
import com.keyportsolutions.security.auth.jwt.exception.InvalidJwtToken;
import com.keyportsolutions.security.auth.jwt.service.UserService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

/**
 * JWTトークン認証Providerクラス
 */
public class JwtTokenAuthProvider implements AuthenticationProvider {

	/** JWTトークン生成ファクトリ */
	private final JwtTokenFactory tokenFactory;

	/** ユーザサービス */
	private final UserService userService;

	/**
	 * コンストラクタ
	 * @param userService ユーザサービス
	 * @param tokenFactory JWTトークン生成ファクトリ
	 */
	public JwtTokenAuthProvider(UserService userService, JwtTokenFactory tokenFactory) {
		this.tokenFactory = tokenFactory;
		this.userService = userService;
	}

	/**
	 * JWTトークン認証を行います。
	 * @see AuthenticationProvider#authenticate(Authentication)
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		RequestJwtAccessToken rawAccessToken = (RequestJwtAccessToken) authentication.getCredentials();

		Jws<Claims> jwsClaims = rawAccessToken.parseClaims(tokenFactory.getTokenSigningKey());
		String subject = jwsClaims.getBody().getSubject();
		@SuppressWarnings("unchecked")
		List<String> scopes = jwsClaims.getBody().get("scopes", List.class);
		List<GrantedAuthority> authorities = scopes.stream().map(authority -> new SimpleGrantedAuthority(authority))
				.collect(Collectors.toList());

		UserDetails user = userService.getByUsername(subject).orElseThrow(() -> new InvalidJwtToken("invalid token"));
		if (!user.getAuthorities().containsAll(authorities)) {
			throw new InvalidJwtToken("invalid token");
		}
		return new JwtAuthToken(user, authorities);

	}

	/**
	 * @see AuthenticationProvider#supports(Class)
	 */
	@Override
	public boolean supports(Class<?> authentication) {
		return (JwtAuthToken.class.isAssignableFrom(authentication));
	}
}
