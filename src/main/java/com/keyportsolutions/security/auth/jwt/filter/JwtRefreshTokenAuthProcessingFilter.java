package com.keyportsolutions.security.auth.jwt.filter;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import com.keyportsolutions.security.auth.jwt.authtoken.JwtRefreshAuthToken;
import com.keyportsolutions.security.auth.jwt.common.JwtConst;
import com.keyportsolutions.security.auth.jwt.common.JwtHeaderTokenExtractor;
import com.keyportsolutions.security.auth.jwt.dataholder.JwtTokenFactory;
import com.keyportsolutions.security.auth.jwt.dataholder.RequestJwtAccessToken;
import com.keyportsolutions.security.auth.jwt.handler.JwtAuthSuccessHandler;
import com.keyportsolutions.security.auth.jwt.handler.JwtTokenAuthFailureHandler;

/**
 * JWTリフレッシュトークン認証フィルタークラス
 */
public class JwtRefreshTokenAuthProcessingFilter extends AbstractAuthenticationProcessingFilter {

	/**
	 * コンストラクタ
	 * @param factory JWTトークン
	 * @param authenticationManager 認証マネージャー
	 */
	public JwtRefreshTokenAuthProcessingFilter(JwtTokenFactory factory, AuthenticationManager authenticationManager) {
        super(factory.getTokenRefreshEntryPoint());
        setAuthenticationSuccessHandler(new JwtAuthSuccessHandler(factory));
        setAuthenticationFailureHandler(new JwtTokenAuthFailureHandler());
        setAuthenticationManager(authenticationManager);
    }

	/**
	 * 認証処理を行います。
	 * @see AbstractAuthenticationProcessingFilter#attemptAuthentication(HttpServletRequest, HttpServletResponse)
	 */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException, IOException, ServletException {
        String tokenPayload = request.getHeader(JwtConst.JWT_TOKEN_HEADER_PARAM);
        RequestJwtAccessToken token = new RequestJwtAccessToken(JwtHeaderTokenExtractor.extract(tokenPayload));
        return getAuthenticationManager().authenticate(new JwtRefreshAuthToken(token));
    }

}
