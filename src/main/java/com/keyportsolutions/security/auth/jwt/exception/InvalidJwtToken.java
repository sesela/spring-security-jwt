package com.keyportsolutions.security.auth.jwt.exception;

/**
 * JWTトークン無効例外クラス
 */
public class InvalidJwtToken extends RuntimeException {
	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * コンストラクタ
	 */
	public InvalidJwtToken() {
		super();
	}

	/**
	 * コンストラクタ
	 * @param message メッセージ
	 * @param cause 原因
	 */
	public InvalidJwtToken(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * コンストラクタ
	 * @param message メッセージ
	 */
	public InvalidJwtToken(String message) {
		super(message);
	}

	/**
	 * コンストラクタ
	 * @param cause 原因
	 */
	public InvalidJwtToken(Throwable cause) {
		super(cause);
	}

}
