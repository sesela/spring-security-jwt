package com.keyportsolutions.security.auth.jwt.dataholder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.jsonwebtoken.Claims;

/**
 * JWTアクセストークンクラス
 */
public final class JwtAccessToken implements JwtToken {

	/** 生トークン */
	private final String rawToken;

	/** Claims */
	@JsonIgnore
	private Claims claims;

	/**
	 * コンストラクタ
	 * @param token トークン
	 * @param claims Claims
	 */
	protected JwtAccessToken(final String token, Claims claims) {
		this.rawToken = token;
		this.claims = claims;
	}

	/**
	 * トークンを取得します。
	 * @return トークン
	 */
	public String getToken() {
		return this.rawToken;
	}

	/**
	 * Claimsを取得します。
	 * @return Claims
	 */
	public Claims getClaims() {
		return claims;
	}
}
