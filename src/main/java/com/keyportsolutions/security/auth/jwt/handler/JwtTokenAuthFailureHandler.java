package com.keyportsolutions.security.auth.jwt.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.keyportsolutions.security.auth.jwt.common.ErrorCode;
import com.keyportsolutions.security.auth.jwt.dataholder.ErrorResponse;
import com.keyportsolutions.security.auth.jwt.exception.AuthMethodNotSupportedException;
import com.keyportsolutions.security.auth.jwt.exception.JwtExpiredTokenException;

/**
 * JWTトークン認証失敗ハンドラクラス
 */
public class JwtTokenAuthFailureHandler implements AuthenticationFailureHandler {

	/** Jsonデータマッパー */
	private final ObjectMapper mapper = new ObjectMapper();

	/**
	 * Httpステータスコードとエラーメッセージをレスポンスに設定します。
	 * @see AuthenticationFailureHandler#onAuthenticationFailure(HttpServletRequest, HttpServletResponse, AuthenticationException)
	 */
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException e) throws IOException, ServletException {

		response.setStatus(HttpStatus.UNAUTHORIZED.value());
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);

		if (e instanceof BadCredentialsException) {
			mapper.writeValue(response.getWriter(),
					ErrorResponse.createErrorResponse("Invalid token",
							ErrorCode.AUTHENTICATION, HttpStatus.UNAUTHORIZED));
		} else if (e instanceof JwtExpiredTokenException) {
			mapper.writeValue(response.getWriter(),
					ErrorResponse.createErrorResponse("Token has expired",
							ErrorCode.JWT_TOKEN_EXPIRED, HttpStatus.UNAUTHORIZED));
		} else if (e instanceof AuthMethodNotSupportedException) {
			mapper.writeValue(response.getWriter(),
					ErrorResponse.createErrorResponse(e.getMessage(),
							ErrorCode.AUTHENTICATION, HttpStatus.UNAUTHORIZED));
		}
		mapper.writeValue(response.getWriter(),
				ErrorResponse.createErrorResponse("Authentication failed",
						ErrorCode.AUTHENTICATION, HttpStatus.UNAUTHORIZED));
	}
}
