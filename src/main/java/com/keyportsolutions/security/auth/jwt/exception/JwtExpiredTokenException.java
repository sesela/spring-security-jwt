package com.keyportsolutions.security.auth.jwt.exception;

import org.springframework.security.core.AuthenticationException;

import com.keyportsolutions.security.auth.jwt.dataholder.JwtToken;

/**
 * JWTトークン失効例外クラス
 */
public class JwtExpiredTokenException extends AuthenticationException {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** JWTトークン */
	private JwtToken token;

	/**
	 * コンストラクタ
	 * @param msg メッセージ
	 */
	public JwtExpiredTokenException(String msg) {
		super(msg);
	}

	/**
	 * コンストラクタ
	 * @param token トークン
	 * @param msg メッセージ
	 * @param e 例外
	 */
	public JwtExpiredTokenException(JwtToken token, String msg, Throwable e) {
		super(msg, e);
		this.token = token;
	}

	/**
	 * トークンを取得します。
	 * @return トークン
	 */
	public String token() {
		return this.token.getToken();
	}
}
