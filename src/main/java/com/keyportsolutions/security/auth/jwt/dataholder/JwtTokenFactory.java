package com.keyportsolutions.security.auth.jwt.dataholder;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.keyportsolutions.security.auth.jwt.common.JwtScopeEnum;
import com.keyportsolutions.security.auth.jwt.config.JwtSettings;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * トークン生成クラス
 */
@Component
public class JwtTokenFactory {

	/** デフォルトアクセストークン失効時間(分) */
	private static final int DEFAULT_ACCESS_TOKEN_EXP_TIME = 15;

	/** デフォルトリフレッシュトークン失効時間(分) */
	private static final int DEFAULT_REFRESH_TOKEN_EXP_TIME = 60;

	/** JWTコンフィグ */
	private final JwtSettings settings;

	/**
	 * コンストラクタ
	 * @param settings JWTコンフィグ
	 */
	@Autowired
	public JwtTokenFactory(JwtSettings settings) {
		this.settings = settings;
	}

	/**
	 * トークン認証対象のエントリーポイントを取得します。
	 * @return トークン認証対象のエントリーポイント
	 */
	public String getTokenBasedAuthEntryPoint() {
		return StringUtils.defaultString(settings.getTokenBasedAuthEntryPoint(), "/**");
	}

	/**
	 * アクセストークン取得エントリーポイントを取得します。
	 * @return アクセストークン取得エントリーポイント
	 */
	public String getTokenCreateEntryPoint() {
		return StringUtils.defaultString(settings.getTokenCreateEntryPoint(), "/api/auth/token");
	}

	/**
	 * トークンリフレッシュ処理エントリーポイントを取得します。
	 * @return トークンリフレッシュ処理エントリーポイント
	 */
	public String getTokenRefreshEntryPoint() {
		return StringUtils.defaultString(settings.getTokenRefreshEntryPoint(), "/api/auth/refresh");
	}

	/**
	 * アクセストークン失効時間を取得します。
	 * @return アクセストークン失効時間
	 */
	public int getAccessTokenExpTime() {
		return NumberUtils.toInt(settings.getAccessTokenExpTime(), DEFAULT_ACCESS_TOKEN_EXP_TIME);
	}

	/**
	 * リフレッシュトークン失効時間を取得します。
	 * @return リフレッシュトークン失効時間
	 */
	public int getRefreshTokenExpTime() {
		return NumberUtils.toInt(settings.getRefreshTokenExpTime(), DEFAULT_REFRESH_TOKEN_EXP_TIME);
	}

	/**
	 * トークン発行者を取得します。
	 * @return トークン発行者
	 */
	public String getTokenIssuer() {
		return StringUtils.defaultString(settings.getTokenIssuer(), "keyportIssue");
	}

	/**
	 * 署名キーを取得します。
	 * @return 署名キー
	 */
	public String getTokenSigningKey() {
		return StringUtils.defaultString(settings.getTokenSigningKey(), "xm8EV6Hy5RMFK4EEACIDAwQus");
	}

	/**
	 * アクセストークンを作成します。
	 *
	 * @param userContext ユーザコンテキスト
	 * @return アクセストークン
	 */
	public JwtAccessToken createAccessJwtToken(UserDetails userContext) {
		if (StringUtils.isBlank(userContext.getUsername())) {
			throw new IllegalArgumentException("Cannot create JWT Token without username");
		}
		if (userContext.getAuthorities() == null || userContext.getAuthorities().isEmpty()) {
			throw new IllegalArgumentException("User doesn't have any privileges");
		}
		Claims claims = Jwts.claims().setSubject(userContext.getUsername());
		claims.put("scopes", userContext.getAuthorities().stream().map(s -> s.toString()).collect(Collectors.toList()));

		LocalDateTime currentTime = LocalDateTime.now();
		ZonedDateTime currentZdt = currentTime.atZone(ZoneId.systemDefault());
		ZonedDateTime expirationZdt = currentTime.plusMinutes(getAccessTokenExpTime())
				.atZone(ZoneId.systemDefault());

		String token = Jwts.builder().setClaims(claims)
				.setIssuedAt(Date.from(currentZdt.toInstant())).setExpiration(Date.from(expirationZdt.toInstant()))
				.setIssuer(settings.getTokenIssuer()).signWith(SignatureAlgorithm.HS512, settings.getTokenSigningKey())
				.compact();

		return new JwtAccessToken(token, claims);
	}

	/**
	 * リフレッシュトークンを作成します。
	 * @param userContext ユーザコンテキスト
	 * @return リフレッシュトークン
	 */
	public JwtToken createRefreshToken(UserDetails userContext) {
		if (StringUtils.isBlank(userContext.getUsername())) {
			throw new IllegalArgumentException("Cannot create JWT Token without username");
		}
		Claims claims = Jwts.claims().setSubject(userContext.getUsername());
		claims.put("scopes", Arrays.asList(JwtScopeEnum.REFRESH_TOKEN.authority()));

		LocalDateTime currentTime = LocalDateTime.now();
		ZonedDateTime currentZdt = currentTime.atZone(ZoneId.systemDefault());
		ZonedDateTime expirationZdt = currentTime.plusMinutes(getRefreshTokenExpTime())
				.atZone(ZoneId.systemDefault());

		String token = Jwts.builder().setClaims(claims).setIssuer(settings.getTokenIssuer())
				.setId(UUID.randomUUID().toString())
				.setIssuedAt(Date.from(currentZdt.toInstant())).setExpiration(Date.from(expirationZdt.toInstant()))
				.signWith(SignatureAlgorithm.HS512, settings.getTokenSigningKey()).compact();

		return new JwtAccessToken(token, claims);
	}
}
