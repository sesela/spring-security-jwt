package com.keyportsolutions.security.auth.jwt.filter;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.keyportsolutions.security.auth.jwt.common.WebUtil;
import com.keyportsolutions.security.auth.jwt.dataholder.JwtTokenFactory;
import com.keyportsolutions.security.auth.jwt.dataholder.UserPasswordAuthRequest;
import com.keyportsolutions.security.auth.jwt.exception.AuthMethodNotSupportedException;
import com.keyportsolutions.security.auth.jwt.handler.JwtUserPasswordAuthFailureHandler;
import com.keyportsolutions.security.auth.jwt.handler.JwtAuthSuccessHandler;

/**
 * JWTユーザパスワード認証フィルタークラス
 */
public class JwtUserPasswordProcessingFilter extends AbstractAuthenticationProcessingFilter {

	/** ログ */
	private static Logger logger = LoggerFactory.getLogger(JwtUserPasswordProcessingFilter.class);

	/** Jsonデータマッパー */
	private final ObjectMapper objectMapper = new ObjectMapper();

	/**
	 * コンストラクタ
	 * @param factory JWT生成ファクトリ
	 * @param authenticationManager 認証マネージャ
	 */
	public JwtUserPasswordProcessingFilter(JwtTokenFactory factory, AuthenticationManager authenticationManager) {
		super(factory.getTokenCreateEntryPoint());
		setAuthenticationSuccessHandler(new JwtAuthSuccessHandler(factory));
		setAuthenticationFailureHandler(new JwtUserPasswordAuthFailureHandler());
		setAuthenticationManager(authenticationManager);
	}

	/**
	 * 認証処理を行います。
	 * @see AbstractAuthenticationProcessingFilter#attemptAuthentication(HttpServletRequest, HttpServletResponse)
	 */
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {
		if (!HttpMethod.POST.name().equals(request.getMethod()) || !WebUtil.isAjax(request)) {
			if (logger.isDebugEnabled()) {
				logger.debug("Authentication method not supported. Request method: " + request.getMethod());
			}
			throw new AuthMethodNotSupportedException("Authentication method not supported");
		}
		UserPasswordAuthRequest loginRequest = objectMapper.readValue(request.getReader(), UserPasswordAuthRequest.class);
		if (StringUtils.isBlank(loginRequest.getUsername()) || StringUtils.isBlank(loginRequest.getPassword())) {
			throw new AuthenticationServiceException("Username or Password not provided");
		}
		UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),
				loginRequest.getPassword());
		return this.getAuthenticationManager().authenticate(token);
	}

}
