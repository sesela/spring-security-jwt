package com.keyportsolutions.security.auth.jwt.common;

/**
 * JWTスコープEnum
 */
public enum JwtScopeEnum {

	/** リフレッシュトークン */
	REFRESH_TOKEN;

	/**
	 * スコープ権限を取得します。
	 * @return スコープ権限　
	 */
	public String authority() {
		return this.name();
	}
}
