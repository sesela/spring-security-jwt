package com.keyportsolutions.security.auth.jwt.dataholder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;

import com.keyportsolutions.security.auth.jwt.exception.JwtExpiredTokenException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

/**
 * JWTアクセストークン格納クラス
 */
public class RequestJwtAccessToken implements JwtToken {

	/** ログ */
	private static Logger logger = LoggerFactory.getLogger(RequestJwtAccessToken.class);

	/** トークン */
	private String token;

	/**
	 * コンストラクタ
	 * @param token トークン
	 */
	public RequestJwtAccessToken(String token) {
		this.token = token;
	}


	/**
	 * 指定した署名キーでトークンを解析してJsonWebトークンを生成します。
	 * @param signingKey 署名キー
	 * @return JsonWebトークン
	 * @exception BadCredentialsException トークン不正例外
	 * @exception JwtExpiredTokenException トークン失効例外
	 */
	public Jws<Claims> parseClaims(String signingKey) {
		try {
			return Jwts.parser().setSigningKey(signingKey).parseClaimsJws(token);
		} catch (UnsupportedJwtException | MalformedJwtException | IllegalArgumentException | SignatureException ex) {
			if (logger.isDebugEnabled()) {
				logger.debug("Invalid JWT Token", ex);
			}
			throw new BadCredentialsException("Invalid JWT token: ", ex);
		} catch (ExpiredJwtException expiredEx) {
			if (logger.isDebugEnabled()) {
				logger.debug("JWT Token is expired", expiredEx);
			}
			throw new JwtExpiredTokenException(this, "JWT Token expired", expiredEx);
		}
	}

	@Override
	public final String getToken() {
		return token;
	}
}
