package com.keyportsolutions.security.auth.jwt.authtoken;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import com.keyportsolutions.security.auth.jwt.dataholder.RequestJwtAccessToken;

/**
 * JWT用既定AuthenticationTokenクラス
 */
public abstract class AbstractJwtAuthToken extends AbstractAuthenticationToken {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/** 生アクセストークン */
	private RequestJwtAccessToken rawAccessToken;

	/** ユーザコンテキスト */
	private Object principal;

	/**
	 * コンストラクタ
	 * @param rawAccessJwtToken 生アクセストークン
	 */
	public AbstractJwtAuthToken(RequestJwtAccessToken rawAccessJwtToken) {
		super(null);
		this.rawAccessToken = rawAccessJwtToken;
		setAuthenticated(false);
	}

	/**
	 * コンストラクタ
	 * @param princial ユーザコンテキスト
	 * @param authorities ユーザ付与権限
	 */
	public AbstractJwtAuthToken(Object princial, Collection<? extends GrantedAuthority> authorities) {
		super(authorities);
		this.eraseCredentials();
		this.principal = princial;
		super.setAuthenticated(true);
	}

	/**
	 * @see AbstractAuthenticationToken#setAuthenticated(boolean authenticated)
	 */
	@Override
	public void setAuthenticated(boolean authenticated) {
		if (authenticated) {
			throw new IllegalArgumentException(
					"Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
		}
		super.setAuthenticated(false);
	}

	/**
	 * @see AbstractAuthenticationToken#getCredentials()
	 */
	@Override
	public Object getCredentials() {
		return rawAccessToken;
	}

	/**
	 * @see AbstractAuthenticationToken#getPrincipal()
	 */
	@Override
	public Object getPrincipal() {
		return principal;
	}

	/**
	 * @see AbstractAuthenticationToken#eraseCredentials()
	 */
	@Override
	public void eraseCredentials() {
		super.eraseCredentials();
		rawAccessToken = null;
	}

}
