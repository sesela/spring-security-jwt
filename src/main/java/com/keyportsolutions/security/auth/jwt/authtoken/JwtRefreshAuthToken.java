package com.keyportsolutions.security.auth.jwt.authtoken;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import com.keyportsolutions.security.auth.jwt.dataholder.RequestJwtAccessToken;

/**
 * JWT用リフレッシュAuthenticationTokenクラス
 */
public class JwtRefreshAuthToken extends AbstractJwtAuthToken {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * コンストラクタ
	 * @param rawAccessJwtToken 生アクセストークン
	 */
	public JwtRefreshAuthToken(RequestJwtAccessToken rawAccessJwtToken) {
		super(rawAccessJwtToken);
	}

	/**
	 * コンストラクタ
	 * @param princial ユーザコンテキスト
	 * @param authorities ユーザ付与権限
	 */
	public JwtRefreshAuthToken(Object princial, Collection<? extends GrantedAuthority> authorities) {
		super(princial, authorities);
	}

}
