package com.keyportsolutions.security.auth.jwt.service;

import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * ユーザサービス
 */
public interface UserService {

	/**
	 * 指定した引数に紐づくユーザ情報を取得します。
	 * @param username ユーザ名
	 * @return ユーザ情報
	 */
	Optional<UserDetails> getByUsername(String username);
}
