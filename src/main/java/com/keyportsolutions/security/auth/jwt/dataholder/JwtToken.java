package com.keyportsolutions.security.auth.jwt.dataholder;

/**
 * JWTトークンインターフェース
 */
public interface JwtToken {

	/**
	 * トークンを取得します。
	 * @return トークン
	 */
    String getToken();
}
