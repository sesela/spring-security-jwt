package com.keyportsolutions.security.auth.jwt.handler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.keyportsolutions.security.auth.jwt.dataholder.JwtToken;
import com.keyportsolutions.security.auth.jwt.dataholder.JwtTokenFactory;

/**
 * JWT認証成功ハンドラクラス
 */
public class JwtAuthSuccessHandler implements AuthenticationSuccessHandler {

	/** Jsonデータマッパー */
	private final ObjectMapper mapper = new ObjectMapper();

	/** JWTトークン生成ファクトリ */
	private final JwtTokenFactory tokenFactory;

	/**
	 * コンストラクタ
	 * @param tokenFactory JWTトークン生成ファクトリ
	 */
	public JwtAuthSuccessHandler(final JwtTokenFactory tokenFactory) {
		this.tokenFactory = tokenFactory;
	}

	/**
	 * アクセストークンとリフレッシュトークンを生成してレスポンスに設定します。
	 * @see AuthenticationSuccessHandler#onAuthenticationSuccess(HttpServletRequest, HttpServletResponse, Authentication)
	 */
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		UserDetails userContext = (UserDetails) authentication.getPrincipal();

		JwtToken accessToken = tokenFactory.createAccessJwtToken(userContext);
		JwtToken refreshToken = tokenFactory.createRefreshToken(userContext);

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("token", accessToken.getToken());
		tokenMap.put("refreshToken", refreshToken.getToken());

		response.setStatus(HttpStatus.OK.value());
		response.setContentType(MediaType.APPLICATION_JSON_VALUE);
		mapper.writeValue(response.getWriter(), tokenMap);

		clearAuthenticationAttributes(request);
	}

	/**
	 * ー時的な認証関連のデータを削除します。
	 * @param request リクエスト
	 */
	protected void clearAuthenticationAttributes(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session == null) {
			return;
		}
		session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
	}
}
