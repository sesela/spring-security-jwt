package com.keyportsolutions.security.auth.jwt.exception;

import org.springframework.security.authentication.AuthenticationServiceException;

/**
 * 認証対象メソッド不正例外クラス
 */
public class AuthMethodNotSupportedException extends AuthenticationServiceException {

	/** serialVersionUID */
	private static final long serialVersionUID = 1L;

	/**
	 * コンストラクタ
	 * @param msg メッセージ
	 */
	public AuthMethodNotSupportedException(String msg) {
		super(msg);
	}
}
