package com.keyportsolutions.security.auth.jwt.provider;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.Assert;

import com.keyportsolutions.security.auth.jwt.service.UserService;

/**
 * ユーザパスワード認証Providerクラス
 */
public class JwtUserPasswordAuthProvider implements AuthenticationProvider {

	/** パスワードエンコーダ */
	private final PasswordEncoder encoder;

	/** ユーザ取得サービス */
	private final UserService userService;

	/**
	 * コンストラクタ
	 * @param userService ユーザ取得サービス
	 * @param encoder パスワードエンコーダ
	 */
	public JwtUserPasswordAuthProvider(final UserService userService, final PasswordEncoder encoder) {
		this.userService = userService;
		this.encoder = encoder;
	}

	/**
	 * ユーザパスワード認証を行います。
	 * @see AuthenticationProvider#authenticate(Authentication)
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		Assert.notNull(authentication, "No authentication data provided");

		String username = (String) authentication.getPrincipal();
		String password = (String) authentication.getCredentials();

		UserDetails user = userService.getByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("User not found: " + username));

		if (!encoder.matches(password, user.getPassword())) {
			throw new BadCredentialsException("Authentication Failed. Username or Password not valid.");
		}

		if (user.getAuthorities() == null) {
			throw new InsufficientAuthenticationException("User has no roles assigned");
		}
		return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
	}

	/**
	 * @see AuthenticationProvider#supports(Class)
	 */
	@Override
	public boolean supports(Class<?> authentication) {
		return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}
}
