package com.keyportsolutions.security.auth.jwt.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * JWTコンフィグクラス
 */
@Configuration
@ConfigurationProperties(prefix = "api.security.jwt")
public class JwtSettings {

	/** トークン発行者 */
	private String tokenIssuer;
	/** 署名キー */
	private String tokenSigningKey;
	/** アクセストークン失効時間(分) */
	private String accessTokenExpTime;
	/** リフレッシュトークン失効時間(分) */
	private String refreshTokenExpTime;
	/** トークン認証対象のエントリーポイント */
	private String tokenBasedAuthEntryPoint;
	/** トークンリフレッシュ処理エントリーポイント */
	private String tokenRefreshEntryPoint;
	/** アクセストークン取得エントリーポイント */
	private String tokenCreateEntryPoint;

	/**
	 * トークン発行者を取得します。
	 * @return トークン発行者
	 */
	public String getTokenIssuer() {
		return tokenIssuer;
	}

	/**
	 * トークン発行者を設定します。
	 * @param tokenIssuer トークン発行者
	 */
	public void setTokenIssuer(String tokenIssuer) {
		this.tokenIssuer = tokenIssuer;
	}

	/**
	 * 署名キーを取得します。
	 * @return 署名キー
	 */
	public String getTokenSigningKey() {
		return tokenSigningKey;
	}

	/**
	 * 署名キーを設定します。
	 * @param tokenSigningKey 署名キー
	 */
	public void setTokenSigningKey(String tokenSigningKey) {
		this.tokenSigningKey = tokenSigningKey;
	}

	/**
	 * アクセストークン失効時間を取得します。
	 * @return アクセストークン失効時間
	 */
	public String getAccessTokenExpTime() {
		return accessTokenExpTime;
	}

	/**
	 * アクセストークン失効時間を設定します。
	 * @param accessTokenExpTime アクセストークン失効時間
	 */
	public void setAccessTokenExpTime(String accessTokenExpTime) {
		this.accessTokenExpTime = accessTokenExpTime;
	}

	/**
	 * リフレッシュトークン失効時間を取得します。
	 * @return リフレッシュトークン失効時間
	 */
	public String getRefreshTokenExpTime() {
		return refreshTokenExpTime;
	}

	/**
	 * リフレッシュトークン失効時間を設定します。
	 * @param refreshTokenExpTime リフレッシュトークン失効時間
	 */
	public void setRefreshTokenExpTime(String refreshTokenExpTime) {
		this.refreshTokenExpTime = refreshTokenExpTime;
	}

	/**
	 * トークン認証対象のエントリーポイントを取得します。
	 * @return トークン認証対象のエントリーポイント
	 */
	public String getTokenBasedAuthEntryPoint() {
		return tokenBasedAuthEntryPoint;
	}

	/**
	 * トークン認証対象のエントリーポイントを設定します。
	 * @param tokenBasedAuthEntryPoint トークン認証対象のエントリーポイント
	 */
	public void setTokenBasedAuthEntryPoint(String tokenBasedAuthEntryPoint) {
		this.tokenBasedAuthEntryPoint = tokenBasedAuthEntryPoint;
	}

	/**
	 * トークンリフレッシュ処理エントリーポイントを取得します。
	 * @return トークンリフレッシュ処理エントリーポイント
	 */
	public String getTokenRefreshEntryPoint() {
		return tokenRefreshEntryPoint;
	}

	/**
	 * トークンリフレッシュ処理エントリーポイントを設定します。
	 * @param tokenRefreshEntryPoint トークンリフレッシュ処理エントリーポイント
	 */
	public void setTokenRefreshEntryPoint(String tokenRefreshEntryPoint) {
		this.tokenRefreshEntryPoint = tokenRefreshEntryPoint;
	}

	/**
	 * アクセストークン取得エントリーポイントを取得します。
	 * @return アクセストークン取得エントリーポイント
	 */
	public String getTokenCreateEntryPoint() {
		return tokenCreateEntryPoint;
	}
	/**
	 * アクセストークン取得エントリーポイントを設定します。
	 * @param tokenCreateEntryPoint アクセストークン取得エントリーポイント
	 */
	public void setTokenCreateEntryPoint(String tokenCreateEntryPoint) {
		this.tokenCreateEntryPoint = tokenCreateEntryPoint;
	}



}
