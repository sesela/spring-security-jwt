package com.keyportsolutions.security.auth.jwt.filter;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import com.keyportsolutions.security.auth.jwt.authtoken.JwtAuthToken;
import com.keyportsolutions.security.auth.jwt.common.JwtConst;
import com.keyportsolutions.security.auth.jwt.common.JwtHeaderTokenExtractor;
import com.keyportsolutions.security.auth.jwt.dataholder.JwtTokenFactory;
import com.keyportsolutions.security.auth.jwt.dataholder.RequestJwtAccessToken;
import com.keyportsolutions.security.auth.jwt.handler.JwtTokenAuthFailureHandler;
import com.keyportsolutions.security.auth.jwt.requestmatcher.SkipPathRequestMatcher;

/**
 * JWTトークン認証フィルタークラス
 */
public class JwtTokenAuthProcessingFilter extends AbstractAuthenticationProcessingFilter {

	/**
	 * コンストラクタ
	 * @param factory JWT生成ファクトリ
	 * @param authenticationManager 認証マネージャ
	 */
	public JwtTokenAuthProcessingFilter(JwtTokenFactory factory, AuthenticationManager authenticationManager) {
		super(new SkipPathRequestMatcher(
				Arrays.asList(factory.getTokenCreateEntryPoint(), factory.getTokenRefreshEntryPoint()),
				factory.getTokenBasedAuthEntryPoint()));
		setAuthenticationFailureHandler(new JwtTokenAuthFailureHandler());
		setAuthenticationSuccessHandler((request, response, authentication) -> { }); // 何もしない。
        setAuthenticationManager(authenticationManager);
	}

	/**
	 * 認証処理を行います。
	 * @see AbstractAuthenticationProcessingFilter#attemptAuthentication(HttpServletRequest, HttpServletResponse)
	 */
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {
		String tokenPayload = request.getHeader(JwtConst.JWT_TOKEN_HEADER_PARAM);
		RequestJwtAccessToken token = new RequestJwtAccessToken(JwtHeaderTokenExtractor.extract(tokenPayload));
		return getAuthenticationManager().authenticate(new JwtAuthToken(token));
	}

	/**
	 * @see AbstractAuthenticationProcessingFilter#successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) // CHECKSTYLE IGNORE
	 */
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		super.successfulAuthentication(request, response, chain, authResult);
		chain.doFilter(request, response);
	}

}
