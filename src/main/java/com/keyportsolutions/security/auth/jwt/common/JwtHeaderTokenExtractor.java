package com.keyportsolutions.security.auth.jwt.common;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationServiceException;

/**
 * JWTヘッダトークン抽出クラス
 */
public final class JwtHeaderTokenExtractor {

	/** ヘッダー接頭辞 */
	public static final String HEADER_PREFIX = "Bearer ";

	/**
	 * コンストラクタ
	 */
	private JwtHeaderTokenExtractor() {
		super();
	}

	/**
	 * JWTヘッダに含まれるトークンを取得します。
	 * @param header JWTヘッダ
	 * @return トークン
	 */
	public static String extract(String header) {
		if (StringUtils.isBlank(header)) {
			throw new AuthenticationServiceException("Authorization header cannot be blank!");
		}
		if (header.length() < HEADER_PREFIX.length()) {
			throw new AuthenticationServiceException("Invalid authorization header size.");
		}
		return header.substring(HEADER_PREFIX.length(), header.length());
	}
}
